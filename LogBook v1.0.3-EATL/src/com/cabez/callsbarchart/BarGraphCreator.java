package com.cabez.callsbarchart;

import java.text.ParseException;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;

import com.cabez.logbook.db.DBAdapter;

public class BarGraphCreator {

	private Cursor mCursor;
	int callDurY[];
	private DBAdapter dbAdapter;

	@SuppressLint("SimpleDateFormat")
	public Intent getIntent(Context context, String number)
			throws ParseException {

		dbCreate(context);

		mCursor = dbAdapter.getCallLogByNumber(number);
		
		int callDurY[] = new int[mCursor.getCount()];
		int yCounter = 0;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor
				.moveToNext()) {

			callDurY[yCounter] = Integer.parseInt(mCursor.getString(mCursor
					.getColumnIndex(DBAdapter.CALL_DURATION)));
			yCounter++;

		}

		CategorySeries series = new CategorySeries("Bar");

		for (int i = 0; i < callDurY.length; i++) {

			series.add("Bar" + (i + 1), callDurY[i]);
		}

		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
		dataSet.addSeries(series.toXYSeries());

		XYSeriesRenderer renderer = new XYSeriesRenderer();
		renderer.setColor(Color.parseColor("#33b5e5"));
		renderer.setDisplayChartValues(true);
		renderer.setChartValuesSpacing((float) 5d);
		renderer.setLineWidth((float) 5d);

		XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
		mRenderer.addSeriesRenderer(renderer);
		mRenderer.setChartTitle("Call Statistic");
		mRenderer.setYTitle("Call Duration (in sec)");
		mRenderer.setZoomButtonsVisible(true);
		mRenderer.setShowLegend(true);
		mRenderer.setShowGridX(true);
		mRenderer.setShowGridY(true);
		mRenderer.setBarSpacing(2);
		mRenderer.setApplyBackgroundColor(true);
		mRenderer.setBackgroundColor(Color.WHITE);
		mRenderer.setXAxisMin(0);
		mRenderer.setXAxisMax(10);
		mRenderer.setYAxisMin(0);
		mRenderer.setYAxisMax(500);
		mRenderer.setLabelsTextSize(20);
		mRenderer.setXLabels(0);
		int counter = 0;
		
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor
				.moveToNext()) {

			String date = mCursor.getString(mCursor
					.getColumnIndex(DBAdapter.CALL_DATE));
			counter++;
			mRenderer.addXTextLabel(counter, "[" + counter + "]");

		}
		mRenderer.setPanEnabled(true, true);
		Intent intent = ChartFactory.getBarChartIntent(context, dataSet,
				mRenderer, Type.DEFAULT);
		return intent;
	}

	private void dbCreate(Context context) {

		dbAdapter = new DBAdapter(context);
		dbAdapter.open();
	}

}
