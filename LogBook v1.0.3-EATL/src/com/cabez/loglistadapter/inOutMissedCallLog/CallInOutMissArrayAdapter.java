package com.cabez.loglistadapter.inOutMissedCallLog;

import java.text.ParseException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.callsbarchart.BarGraphCreator;
import com.cabez.inappmessenger.SendMessage;
import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;

public class CallInOutMissArrayAdapter extends
		ArrayAdapter<ContactCallInOutMissed> {

	Context context;
	int layoutResourceId;
	ArrayList<ContactCallInOutMissed> calls = new ArrayList<ContactCallInOutMissed>();
	Cursor crsr, cursor;
	SQLiteDatabase db;
	private DBAdapter dbAdapter;

	public CallInOutMissArrayAdapter(Context context, int layoutResourceId,
			ArrayList<ContactCallInOutMissed> calls) {
		super(context, layoutResourceId, calls);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.calls = calls;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		ViewWrapper viewWrapper = null;

		if (item == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			item = inflater.inflate(layoutResourceId, parent, false);
			viewWrapper = new ViewWrapper();
			viewWrapper.name = (TextView) item.findViewById(R.id.textName);
			viewWrapper.num = (TextView) item.findViewById(R.id.textAge);
			viewWrapper.date = (TextView) item.findViewById(R.id.textAddr);
			viewWrapper.details = (Button) item.findViewById(R.id.btnEdit);
			viewWrapper.graph = (Button) item.findViewById(R.id.btnDelete);
			viewWrapper.phnCall = (Button) item.findViewById(R.id.btnPhn);
			viewWrapper.duration = (TextView) item.findViewById(R.id.textDur);
			viewWrapper.msg = (Button) item.findViewById(R.id.btnMsg);
			item.setTag(viewWrapper);
		} else {
			viewWrapper = (ViewWrapper) item.getTag();
		}

		dbCreate();
		ContactCallInOutMissed inOutMissedCallLog = calls.get(position);
		viewWrapper.name.setText(inOutMissedCallLog.getName());
		viewWrapper.num.setText(inOutMissedCallLog.getNumber());
		viewWrapper.date.setText(inOutMissedCallLog.getDate());
		viewWrapper.duration.setText(inOutMissedCallLog.getDuration() + " Sec");

		cursor = ContactCallInOutMissed.getCursor();
		final int pos = position;

		viewWrapper.details.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				cursor.moveToPosition(pos);
				String nmbr = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				
				Dialog d = new Dialog(getContext());
				d.setTitle(nmbr + "'s History");
				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

				d.setContentView(R.layout.detail_dialog);
				TextView text = (TextView) d.findViewById(R.id.detail);

				Cursor cr = dbAdapter.getCallLogByNumber(nmbr);
				for (cr.moveToFirst(); !cr.isAfterLast(); cr.moveToNext()) {
					text.append(cr.getString(cr
							.getColumnIndex(DBAdapter.CALL_DATE)) + "\n");
				}
				text.append("Total Calls: " + cr.getCount() + " Times");

				d.show();
			}

		});

		viewWrapper.phnCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				cursor.moveToPosition(pos);
				String number = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));

				try {
					String uri = "tel:" + number;
					Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(uri));

					context.startActivity(dialIntent);

				} catch (Exception e) {

					Toast.makeText(context.getApplicationContext(),
							"Your call has failed...", Toast.LENGTH_LONG)
							.show();

				}
			}
		});

		viewWrapper.msg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				cursor.moveToPosition(pos);
				String num = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));

				Intent intent = new Intent(context, SendMessage.class);
				intent.putExtra("NUMBER", num);
				context.startActivity(intent);

			}
		});

		viewWrapper.graph.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String nmbr = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				BarGraphCreator barChart = new BarGraphCreator();
				Intent barIntent;
				try {
					barIntent = barChart.getIntent(context, nmbr);
					context.startActivity(barIntent);

				} catch (ParseException e) {
					Toast.makeText(context, "ERROR in generating Graph",
							Toast.LENGTH_SHORT).show();
				}
				Toast.makeText(context, "Graph for: " + nmbr, Toast.LENGTH_LONG)
						.show();
			}
		});

		return item;

	}

	private void dbCreate() {
		dbAdapter = new DBAdapter(context);
		dbAdapter.open();
	}

	static class ViewWrapper {
		TextView name;
		TextView num;
		TextView date;
		TextView duration;
		Button phnCall;
		Button msg;
		Button details;
		Button graph;
	}

}
