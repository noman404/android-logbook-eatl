package com.cabez.loglistadapter.inOutMissedCallLog;

import android.content.ContentResolver;
import android.database.Cursor;

public class ContactCallInOutMissed {

	private String name;
	private String number;
	private String date;
	private String duration;

	private static ContentResolver cr;

	private static Cursor cursor;

	public ContactCallInOutMissed(String name, String number, String date, String duration) {
		super();
		this.name = name;
		this.number = number;
		this.date = date;
		this.duration = duration;
	}

	public static ContentResolver getCr() {
		return cr;
	}

	public static void setCr(ContentResolver cr) {
		ContactCallInOutMissed.cr = cr;
	}

	public static Cursor getCursor() {
		return cursor;
	}

	public static void setCursor(Cursor cursor) {
		ContactCallInOutMissed.cursor = cursor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String age) {
		this.number = age;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
