package com.cabez.inappmessenger;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class SendMessage extends Activity {

	private EditText numbs, msgs;
	private Button send;
	private LinearLayout l;
	private Intent i;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		setContentView(R.layout.send_message);
		overridePendingTransition(R.anim.up, R.anim.down);

		i = getIntent();
		final String TO = i.getExtras().getString("NUMBER");

		l = (LinearLayout) findViewById(R.id.ll);
		numbs = (EditText) findViewById(R.id.txtPhoneNo);
		msgs = (EditText) findViewById(R.id.txtMessage);

		if (TO.length() > 0) {
			numbs.setText(TO);
		}

		send = (Button) findViewById(R.id.btnSendSMS);
		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String number, msg;
				number = numbs.getText().toString();
				msg = msgs.getText().toString();

				if (number.length() > 0 && msg.length() > 0) {

					sendSMS(number, msg);
				} else {
					Toast.makeText(SendMessage.this, "Wrong Entry",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	protected void sendSMS(String number, String msg) {

		String SENT = "SMS_SENT";

		PendingIntent sentPI = PendingIntent.getBroadcast(SendMessage.this, 0,
				new Intent(SENT), 0);

		registerReceiver(new BroadcastReceiver() {

			@Override
			public void onReceive(Context c, Intent i) {

				switch (getResultCode()) {

				case Activity.RESULT_OK:
					Toast.makeText(SendMessage.this, "SMS SENT",
							Toast.LENGTH_SHORT).show();
					break;

				default:
					Toast.makeText(SendMessage.this, "FAILED TO SENT",
							Toast.LENGTH_SHORT).show();
					break;
				}

			}
		}, new IntentFilter(SENT));

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(number, null, msg, sentPI, null);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			overridePendingTransition(R.anim.in, R.anim.out);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
