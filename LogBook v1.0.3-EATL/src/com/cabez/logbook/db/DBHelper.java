package com.cabez.logbook.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "CallSMSLog.sqlite";
	protected Context mContext;

	public DBHelper(Context mContext) {
		super(mContext, DB_NAME, null, 5);
		this.mContext = mContext;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("CREATE TABLE CALL(_id INTEGER," + " number TEXT,"
				+ " name TEXT," + " date TEXT," + " call_type TEXT,"
				+ " duration TEXT," + " date_time TEXT NOT NULL PRIMARY KEY)");

		db.execSQL("CREATE TABLE SMS(_id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ " number TEXT,"+" name TEXT," + " sms_type TEXT," + " body TEXT,"
				+ " date TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS CALL");
		db.execSQL("DROP TABLE IF EXISTS SMS");
		onCreate(db);

	}

}
