package com.cabez.logbook.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBAdapter {

	// common table attbrt
	public static final String NAME = "name";
	public static final String ID = "_id";

	// CALL table attributes
	public static final String CALL_NUM = "number";
	public static final String CALL_DATE = "date";
	public static final String CALL_TYPE = "call_type";
	public static final String CALL_DURATION = "duration";
	public static final String CALL_DATE_TIME = "date_time";

	// SMS table attributes
	public static final String SMS_NUMBER = "number";
	public static final String SMS_BODY = "body";
	public static final String SMS_TYPE = "sms_type";
	public static final String SMS_DATE = "date";

	private Context mContext;
	private DBHelper dbHelper;
	public SQLiteDatabase db;

	public DBAdapter(Context mContext) {

		this.mContext = mContext;
	}

	public DBAdapter open() throws SQLException {

		dbHelper = new DBHelper(mContext);
		db = dbHelper.getWritableDatabase();
		return this;
	}

	public long createCalldb(long id, String number, String name, String date,
			String callType, String duration, String dateTime) {

		ContentValues values = new ContentValues();
		values.put(ID, id);
		values.put(CALL_NUM, number);
		values.put(NAME, name);
		values.put(CALL_DATE, date);
		values.put(CALL_TYPE, callType);
		values.put(CALL_DURATION, duration);
		values.put(CALL_DATE_TIME, dateTime);

		return (db.insert("CALL", null, values));
	}

	public long createSMSdb(String number, String name, String body,
			String smsType, String date) {

		ContentValues values = new ContentValues();
		values.put(NAME, name);
		values.put(SMS_NUMBER, number);
		values.put(SMS_BODY, body);
		values.put(SMS_TYPE, smsType);
		values.put(SMS_DATE, date);

		return (db.insert("SMS", null, values));
	}

	public Cursor getAllCallLog() {

		return (db.query(true, "CALL", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.CALL_NUM, DBAdapter.CALL_DATE,
				DBAdapter.CALL_DURATION, DBAdapter.CALL_TYPE,
				DBAdapter.CALL_DATE_TIME }, null, null, DBAdapter.CALL_NUM,
				null, null, null));
	}

	public Cursor getInCallLog() {

		String where = DBAdapter.CALL_TYPE + "=" + "'INCOMING'";

		return (db.query(true, "CALL", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.CALL_NUM, DBAdapter.CALL_DATE,
				DBAdapter.CALL_DURATION, DBAdapter.CALL_TYPE,
				DBAdapter.CALL_DATE_TIME }, where, null, DBAdapter.CALL_NUM,
				null, null, null));
	}

	public Cursor getOutCallLog() {

		String where = DBAdapter.CALL_TYPE + "=" + "'OUTGOING'";

		Cursor c = db.query(true, "CALL", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.CALL_NUM, DBAdapter.CALL_DATE,
				DBAdapter.CALL_DURATION, DBAdapter.CALL_TYPE,
				DBAdapter.CALL_DATE_TIME }, where, null, DBAdapter.CALL_NUM,
				null, null, null);

		if (c.getCount() > 0) {
			c.moveToFirst();
		}

		return c;
	}

	public Cursor getMissedCallLog() {

		String where = DBAdapter.CALL_TYPE + "=" + "'MISSED'";

		return (db.query(true, "CALL", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.CALL_NUM, DBAdapter.CALL_DATE,
				DBAdapter.CALL_DURATION, DBAdapter.CALL_TYPE,
				DBAdapter.CALL_DATE_TIME }, where, null, DBAdapter.CALL_NUM,
				null, null, null));
	}

	public Cursor getAllSMSLog() {

		return (db.query(true, "SMS", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.SMS_NUMBER, DBAdapter.SMS_BODY,
				DBAdapter.SMS_TYPE, DBAdapter.SMS_DATE }, null, null,
				DBAdapter.SMS_NUMBER, null, null, null));
	}

	public Cursor getInboxSMSLog() {

		String where = DBAdapter.SMS_TYPE + "=" + "'INBOX'";
		return (db.query(true, "SMS", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.SMS_NUMBER, DBAdapter.SMS_BODY,
				DBAdapter.SMS_TYPE, DBAdapter.SMS_DATE }, where, null,
				DBAdapter.SMS_NUMBER, null, null, null));
	}

	public Cursor getDraftSMSLog() {

		String where = DBAdapter.SMS_TYPE + "=" + "'DRAFT'";
		return (db.query(true, "SMS", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.SMS_NUMBER, DBAdapter.SMS_BODY,
				DBAdapter.SMS_TYPE, DBAdapter.SMS_DATE }, where, null,
				DBAdapter.SMS_NUMBER, null, null, null));
	}

	public Cursor getSendSMSLog() {

		String where = DBAdapter.SMS_TYPE + "=" + "'SENT'";
		return (db.query(true, "SMS", new String[] { DBAdapter.ID,
				DBAdapter.NAME, DBAdapter.SMS_NUMBER, DBAdapter.SMS_BODY,
				DBAdapter.SMS_TYPE, DBAdapter.SMS_DATE }, where, null,
				DBAdapter.SMS_NUMBER, null, null, null));

	}

	public Cursor getCallLogByNumber(String number) {

		String where = DBAdapter.CALL_NUM + "='" + number + "'";

		return (db
				.query("CALL", new String[] { DBAdapter.ID, DBAdapter.NAME,
						DBAdapter.CALL_NUM, DBAdapter.CALL_DATE,
						DBAdapter.CALL_DURATION, DBAdapter.CALL_TYPE,
						DBAdapter.CALL_DATE_TIME }, where, null, null, null,
						null, null));

	}

	public Cursor getSMSLogByNumber(String number) {

		String where = DBAdapter.SMS_NUMBER + "='" + number + "'";

		return (db.query("SMS", new String[] { DBAdapter.ID, DBAdapter.NAME,
				DBAdapter.SMS_NUMBER, DBAdapter.SMS_BODY, DBAdapter.SMS_TYPE,
				DBAdapter.SMS_DATE }, where, null, null, null, null, null));

	}

	public void close() {

		db.close();
	}

	public boolean isCalldbEmpty() {

		Cursor cursor = db.query("CALL", new String[] { ID }, null, null, null,
				null, null);

		if (cursor != null && cursor.getCount() > 0) {
			return false;
		}
		return true;
	}

	public boolean isSMSdbEmpty() {

		Cursor cursor = db.query("SMS", new String[] { ID }, null, null, null,
				null, null);

		if (cursor != null && cursor.getCount() > 0) {
			return false;
		}
		return true;
	}

	public void resetCalldb() {

		db.delete("CALL", null, null);
	}

	public void resetSMSdb() {

		db.delete("SMS", null, null);
	}

	public Cursor getCallLogCounter(String number) {

		Cursor cursor = db.rawQuery("SELECT * FROM CALL WHERE number LIKE '%"
				+ number + "%' LIMIT 0,1", null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
		}

		return cursor;
	}

	public Cursor getSMSLogCounter(String number) {

		Cursor cursor = db.rawQuery("SELECT * FROM SMS WHERE number LIKE '%"
				+ number + "%' LIMIT 0,1", null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
		}

		return cursor;
	}

	public int getTotalIncoming(String number) {

		Cursor cursor = db.rawQuery(
				"SELECT COUNT(call_type) FROM CALL WHERE number LIKE '%"
						+ number + "%' AND call_type = 'INCOMING'", null);

		cursor.moveToFirst();
		return (cursor.getInt(0));

	}

	public int getTotalOutgoing(String number) {

		Cursor cursor = db.rawQuery(
				"SELECT COUNT(call_type) FROM CALL WHERE number LIKE '%"
						+ number + "%' AND call_type = 'OUTGOING'", null);

		cursor.moveToFirst();
		return (cursor.getInt(0));

	}

	public int getTotalMissed(String number) {

		Cursor cursor = db.rawQuery(
				"SELECT COUNT(call_type) FROM CALL WHERE number LIKE '%"
						+ number + "%' AND call_type = 'MISSED'", null);

		cursor.moveToFirst();
		return (cursor.getInt(0));

	}

	public int getTotalInbox(String number) {

		Cursor cursor = db.rawQuery(
				"SELECT COUNT(sms_type) FROM SMS WHERE number LIKE '%" + number
						+ "%' AND sms_type = 'INBOX'", null);

		cursor.moveToFirst();
		return (cursor.getInt(0));

	}

	public int getTotalOutbox(String number) {

		Cursor cursor = db.rawQuery(
				"SELECT COUNT(sms_type) FROM SMS WHERE number LIKE '%" + number
						+ "%' AND sms_type = 'SENT'", null);

		cursor.moveToFirst();
		return (cursor.getInt(0));

	}

}
