package com.cabez.logbook.fileexplorer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteMisuseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CallLog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.cabez.logbook.main.MainActivity;

public class FileExplorer extends ListActivity {

	private List<String> item = null;
	private List<String> path = null;
	private String root;
	private TextView myPath;
	ProgressDialog pd;
	String fileName;
	Button finish;
	private DBAdapter dbAdapter;
	Cursor callCursor, smsCursor;
	Intent i;
	int taskCode;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.file_explorer);
		overridePendingTransition(R.anim.up, R.anim.down);

		dbCreate();

		/**/
		// final Dialog d = new Dialog(FileExplorer.this);
		// d.setContentView(R.layout.demo_dialog);
		// d.setTitle("Demo App");
		// d.setCancelable(false);
		// Button demo = (Button) d.findViewById(R.id.okDemo);
		// demo.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// d.dismiss();
		// }
		// });
		// d.show();
		/**/

		taskCode = getIntent().getIntExtra("TASK", 1);

		myPath = (TextView) findViewById(R.id.path);
		finish = (Button) findViewById(R.id.finishFileLoading);
		finish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(FileExplorer.this, MainActivity.class));
				finish();
			}
		});
		root = Environment.getExternalStorageDirectory().getPath();

		getDir(root);
	}

	private void getDir(String dirPath) {
		myPath.setText("Location: " + dirPath);
		item = new ArrayList<String>();
		path = new ArrayList<String>();
		File f = new File(dirPath);
		File[] files = f.listFiles();

		if (!dirPath.equals(root)) {
			item.add(root);
			path.add(root);
			item.add("../");
			path.add(f.getParent());
		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			if (!file.isHidden() && file.canRead()) {
				path.add(file.getPath());
				if (file.isDirectory()) {
					item.add(file.getName() + "/");
				} else {
					item.add(file.getName());
				}
			}
		}

		ArrayAdapter<String> fileList = new ArrayAdapter<String>(this,
				R.layout.explorer_row, item);
		setListAdapter(fileList);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		File file = new File(path.get(position));

		if (file.isDirectory()) {
			if (file.canRead()) {
				getDir(path.get(position));
			} else {
				new AlertDialog.Builder(this)
						.setIcon(R.drawable.ic_launcher)
						.setTitle(
								"path: " + path.get(position) + file.getName()
										+ " folder can't be read!!!")
						.setPositiveButton("OK", null).show();
			}
		} else {
			fileName = path.get(position);
			myPath.setText(fileName + " selectecd");

			switch (taskCode) {
			case 1:
				if (fileName.toLowerCase().contains(
						"Call_History.xlsx".toLowerCase())) {
					new CopyCallXLSX().execute(fileName);
				} else {
					Toast.makeText(FileExplorer.this, "file Not found!!!",
							Toast.LENGTH_SHORT).show();
					startActivity(new Intent(FileExplorer.this,
							MainActivity.class));
					finish();
				}
				break;
			case 2:
				if (fileName.toLowerCase().contains(
						"SMS_History.xlsx".toLowerCase())) {
					new CopySMSXLSX().execute(fileName);
				} else {
					Toast.makeText(FileExplorer.this, "file Not found!!!",
							Toast.LENGTH_SHORT).show();
					startActivity(new Intent(FileExplorer.this,
							MainActivity.class));
					finish();
				}
				break;

			default:
				break;
			}

		}
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(FileExplorer.this);
		dbAdapter.open();
	}

	class CopySMSXLSX extends AsyncTask<String, Void, Void> {
		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(FileExplorer.this, "Restoring Backup",
					"Please Wait....", true);
		}

		@Override
		protected Void doInBackground(String... arguments) {

			copySMSXLSX(getContentResolver(), arguments[0]);

			return null;
		}

		private void copySMSXLSX(ContentResolver cr, String filePath) {
			try {
				FileInputStream file = new FileInputStream(new File(filePath));
				HSSFWorkbook wb = new HSSFWorkbook(file);
				HSSFSheet sheet = wb.getSheetAt(0);
				Row row = sheet.getRow(0);
				int size = sheet.getLastRowNum() - 1;
				for (int i = 0; i < size; i++) {
					row = sheet.getRow(i);

					String name = row.getCell(1).getStringCellValue();

					String number = row.getCell(2).getStringCellValue();

					String body = row.getCell(3).getStringCellValue();

					String date = row.getCell(4).getStringCellValue();

					String type = row.getCell(5).getStringCellValue();

					try {
						dbAdapter.createSMSdb(number, name, body, type, date);
					} catch (SQLiteAbortException e) {

						continue;
					} catch (SQLiteDoneException e) {

						continue;
					} catch (SQLiteMisuseException e) {

						continue;
					} catch (SQLiteConstraintException e) {

						continue;
					} catch (SQLiteException error) {

						continue;
					} catch (Exception e) {

						continue;
					}
				}
			} catch (IOException ioe) {
			}

			Cursor smsCursor = dbAdapter.getAllSMSLog();
			for (smsCursor.moveToFirst(); !smsCursor.isAfterLast(); smsCursor
					.moveToNext()) {

				String address = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));
				String body = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_BODY));
				String date = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_DATE));
				String type = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_TYPE));

				if (type.equals("INBOX")) {
					insertSMSLog(address, body, "inbox", date);
				}
				if (type.equals("SENT")) {
					insertSMSLog(address, body, "sent", date);
				}
				if (type.equals("DRAFT")) {
					insertSMSLog(address, body, "draft", date);
				}

			}

		}

		private void insertSMSLog(String address, String body, String type,
				String date) {
			ContentValues values = new ContentValues();
			values.put("address", address);
			values.put("body", body);
			values.put("date", date);

			getContentResolver().insert(Uri.parse("content://sms/" + type),
					values);
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();
			Toast.makeText(FileExplorer.this, "imported", Toast.LENGTH_SHORT)
					.show();
			startActivity(new Intent(FileExplorer.this, MainActivity.class));
			finish();

		}

	}

	class CopyCallXLSX extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(FileExplorer.this, "Restoring Backup",
					"Please Wait....", true);
		}

		@Override
		protected Void doInBackground(String... arguments) {
			copyCallXLSX(getContentResolver(), arguments[0]);
			return null;
		}

		void copyCallXLSX(ContentResolver contentResolver, String filePath) {
			try {
				FileInputStream file = new FileInputStream(new File(filePath));
				HSSFWorkbook wb = new HSSFWorkbook(file);
				HSSFSheet sheet = wb.getSheetAt(0);
				Row row = sheet.getRow(0);
				int size = sheet.getLastRowNum() - 1;
				for (int i = 0; i < size; i++) {
					row = sheet.getRow(i);
					String name = row.getCell(1).getStringCellValue();

					String number = row.getCell(2).getStringCellValue();

					String callType = row.getCell(3).getStringCellValue();

					String duration = "" + row.getCell(4).getStringCellValue();

					String date = row.getCell(5).getStringCellValue();

					String dateTime = row.getCell(6).getStringCellValue();

					try {
						dbAdapter.createCalldb(i, number, name, date, callType,
								duration, dateTime);
					} catch (SQLiteAbortException e) {

						continue;
					} catch (SQLiteDoneException e) {

						continue;
					} catch (SQLiteMisuseException e) {

						continue;
					} catch (SQLiteException error) {

						continue;
					} catch (Exception e) {

						continue;
					}
				}
			} catch (IOException ioe) {
			}

			Cursor callCursor = dbAdapter.getAllCallLog();
			String number, callType, duration, dateTime;
			for (callCursor.moveToFirst(); !callCursor.isAfterLast(); callCursor
					.moveToNext()) {

				number = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				callType = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_TYPE));
				duration = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_DURATION));

				dateTime = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_DATE_TIME));
				insertIntoCallLogValues(contentResolver, number, "UNKNOWN",
						dateTime, callType, duration);
			}

		}

		void insertIntoCallLogValues(ContentResolver contentResolver,
				String number, String name, String date, String callType,
				String duration) {

			ContentValues values = new ContentValues();

			values.put(CallLog.Calls.NUMBER, number);

			values.put(CallLog.Calls.CACHED_NAME, name);

			values.put(CallLog.Calls.DATE, date);

			int type = 3;

			if (callType.equals("INCOMING")) {
				type = 1;
			} else if (callType.equals("OUTGOING")) {
				type = 2;
			} else if (callType.equals("MISSED")) {
				type = 3;
			} else {
				type = 3;
			}

			values.put(CallLog.Calls.TYPE, type);

			values.put(CallLog.Calls.DURATION, Long.parseLong(duration));

			contentResolver.insert(CallLog.Calls.CONTENT_URI, values);
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();
			Toast.makeText(FileExplorer.this,
					"imported-please, reoprn the application :-)",
					Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	@Override
	public void onBackPressed() {
		Toast.makeText(FileExplorer.this, "Tap ../ Line To Go Back",
				Toast.LENGTH_SHORT).show();
	}

}
