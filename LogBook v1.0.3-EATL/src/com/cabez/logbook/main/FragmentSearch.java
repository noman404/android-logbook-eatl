package com.cabez.logbook.main;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class FragmentSearch extends Fragment {

	public Cursor cursor;
	public DBAdapter dbAdapter;
	public ProgressDialog pd;
	public Cursor callCursor, smsCursor;
	public TextView tNam, tNum, tIn, tOut, tMis, tInbx, tOutbx;
	public EditText et;
	public Button search;
	public ImageView fb, g, twtr, play, update;

	public FragmentSearch() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_eleven,
				container, false);
		et = (EditText) view.findViewById(R.id.etSearch);
		tNam = (TextView) view.findViewById(R.id.sName);
		tNum = (TextView) view.findViewById(R.id.sNumber);
		tIn = (TextView) view.findViewById(R.id.sTotalInCall);
		tOut = (TextView) view.findViewById(R.id.sTotalOutCall);
		tMis = (TextView) view.findViewById(R.id.sTotalMissedCall);
		tInbx = (TextView) view.findViewById(R.id.sTotalInbox);
		tOutbx = (TextView) view.findViewById(R.id.sTotalOutbox);
		search = (Button) view.findViewById(R.id.btnsearch);

		fb = (ImageView) view.findViewById(R.id.fb);
		g = (ImageView) view.findViewById(R.id.gplus);
		twtr = (ImageView) view.findViewById(R.id.twitter);
		play = (ImageView) view.findViewById(R.id.play);
		update = (ImageView) view.findViewById(R.id.update);

		dbCreate();

		tNam.setVisibility(View.GONE);
		tNum.setVisibility(View.GONE);
		tIn.setVisibility(View.GONE);
		tOut.setVisibility(View.GONE);
		tMis.setVisibility(View.GONE);
		tInbx.setVisibility(View.GONE);
		tOutbx.setVisibility(View.GONE);

		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!et.getText().toString().equals("")
						&& (et.getText().toString().length() >= 8)) {
					String number = et.getText().toString();

					Cursor cCall, cSMS;
					try {
						cCall = dbAdapter.getCallLogCounter(number);
						cSMS = dbAdapter.getSMSLogCounter(number);

						tNam.setVisibility(View.VISIBLE);
						tNum.setVisibility(View.VISIBLE);
						tIn.setVisibility(View.VISIBLE);
						tOut.setVisibility(View.VISIBLE);
						tMis.setVisibility(View.VISIBLE);
						tInbx.setVisibility(View.VISIBLE);
						tOutbx.setVisibility(View.VISIBLE);

						tNam.setText("Name: "
								+ cCall.getString(cCall
										.getColumnIndex(DBAdapter.NAME)));
						tNum.setText("Number: "
								+ cCall.getString(cCall
										.getColumnIndex(DBAdapter.CALL_NUM)));
						tIn.setText("Total Incoming: "
								+ dbAdapter.getTotalIncoming(number));
						tOut.setText("Total Outgoing: "
								+ dbAdapter.getTotalOutgoing(number));
						tMis.setText("Total Missed: "
								+ dbAdapter.getTotalMissed(number));
						tInbx.setText("Total Inbox: "
								+ dbAdapter.getTotalInbox(number));
						tOutbx.setText("Total Send: "
								+ dbAdapter.getTotalOutbox(number));

					} catch (CursorIndexOutOfBoundsException e) {
						Toast.makeText(getActivity(), "NOT FOUND",
								Toast.LENGTH_SHORT).show();

						tNam.setVisibility(View.GONE);
						tNum.setVisibility(View.GONE);
						tIn.setVisibility(View.GONE);
						tOut.setVisibility(View.GONE);
						tMis.setVisibility(View.GONE);
						tInbx.setVisibility(View.GONE);
						tOutbx.setVisibility(View.GONE);
					} catch (Exception e) {
						Toast.makeText(getActivity(), "NOT FOUND",
								Toast.LENGTH_SHORT).show();

						tNam.setVisibility(View.GONE);
						tNum.setVisibility(View.GONE);
						tIn.setVisibility(View.GONE);
						tOut.setVisibility(View.GONE);
						tMis.setVisibility(View.GONE);
						tInbx.setVisibility(View.GONE);
						tOutbx.setVisibility(View.GONE);
					}

				} else {
					Toast.makeText(getActivity(), "Fill up", Toast.LENGTH_SHORT)
							.show();

					tNam.setVisibility(View.GONE);
					tNum.setVisibility(View.GONE);
					tIn.setVisibility(View.GONE);
					tOut.setVisibility(View.GONE);
					tMis.setVisibility(View.GONE);
					tInbx.setVisibility(View.GONE);
					tOutbx.setVisibility(View.GONE);
				}

			}
		});

		fb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_VIEW, Uri
						.parse("https://www.facebook.com/kids.bangladroid"));
//				startActivity(i);
			}
		});

		g.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(
						Intent.ACTION_VIEW,
						Uri.parse("https://plus.google.com/u/0/110141523284580437462/posts"));
//				startActivity(i);
			}
		});
		twtr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW, Uri
						.parse("https://twitter.com/"));
//				startActivity(i);
			}
		});
		play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
//					startActivity(new Intent(
//							Intent.ACTION_VIEW,
//							Uri.parse("market://play.google.com/store/apps/developer?id=Bangla+Droid")));
				} catch (android.content.ActivityNotFoundException err) {
//					startActivity(new Intent(
//							Intent.ACTION_VIEW,
//							Uri.parse("http://play.google.com/store/apps/developer?id=Bangla+Droid")));
				} catch (Exception e) {
//					startActivity(new Intent(
//							Intent.ACTION_VIEW,
//							Uri.parse("http://play.google.com/store/apps/developer?id=Bangla+Droid")));
				}

			}
		});
		update.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				Intent intent = new Intent(Intent.ACTION_SEND);
//				intent.setType("plain/text");
//				intent.putExtra(Intent.EXTRA_EMAIL,
//						new String[] { "cabeztech@gmail.com" });
//				intent.putExtra(Intent.EXTRA_SUBJECT, "LogBook Survey");
//				intent.putExtra(Intent.EXTRA_TEXT, "Need Update");
//				startActivity(Intent.createChooser(intent, "Email Us"));
//				Toast.makeText(getActivity().getApplicationContext(),
//						"Please...Just Send This", 800).show();
			}
		});

		return view;
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(getActivity());
		dbAdapter.open();
	}

}
