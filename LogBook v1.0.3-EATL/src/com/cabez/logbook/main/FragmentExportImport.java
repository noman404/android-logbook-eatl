package com.cabez.logbook.main;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import android.app.Fragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.cabez.logbook.fileexplorer.FileExplorer;

public class FragmentExportImport extends Fragment {

	ProgressDialog pd;
	private Button exportCall, importCall, exportSMS, importSMS;
	private DBAdapter dbAdapter;
	Cursor callCursor, smsCursor;

	public FragmentExportImport() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_nine, container,
				false);

		dbCreate();
		callCursor = dbAdapter.getAllCallLog();
		smsCursor = dbAdapter.getAllSMSLog();

		exportCall = (Button) view.findViewById(R.id.export);
		exportCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new CreateCallXLSX().execute();

			}
		});

		importCall = (Button) view.findViewById(R.id.imprt);
		importCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dbAdapter.resetCalldb();
				Intent i = new Intent(getActivity().getApplicationContext(),
						FileExplorer.class);
				i.putExtra("TASK", 1);
				startActivity(i);
				getActivity().finish();
			}
		});

		exportSMS = (Button) view.findViewById(R.id.exportSMS);
		exportSMS.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new CreateSMSXLSX().execute();
			}
		});

		importSMS = (Button) view.findViewById(R.id.imprtSMS);
		importSMS.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity().getApplicationContext(),
						FileExplorer.class);
				i.putExtra("TASK", 2);
				startActivity(i);
				getActivity().finish();
			}
		});

		return view;
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(getActivity());
		dbAdapter.open();
	}

	class CreateCallXLSX extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "Creating Backup",
					"Please Wait....", true);
		}

		@Override
		protected Void doInBackground(Void... arguments) {

			createCallXLSX();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();
			Toast.makeText(getActivity(), "created", Toast.LENGTH_SHORT).show();
			createNotification();

		}

		public void createNotification() {
			String strtitle = getString(R.string.notificationtitle);

			String strtext = getString(R.string.notificationtext);

			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_GET_CONTENT);
			File file = new File("/sdcard/Log_Book/call");
			intent.setDataAndType(Uri.fromFile(file), "application/file");

			intent.putExtra("title", strtitle);
			intent.putExtra("text", strtext);

			PendingIntent pIntent = PendingIntent.getActivity(getActivity()
					.getApplicationContext(), 0, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					getActivity().getApplicationContext())
					.setSmallIcon(R.drawable.calllog_small_ic)
					.setTicker(getString(R.string.notificationticker))
					.setContentTitle(getString(R.string.notificationtitle))
					.setContentText(getString(R.string.notificationtext))
					.addAction(R.drawable.excel_bck_ic, "Tap to view", pIntent)
					.setContentIntent(pIntent).setAutoCancel(true);

			NotificationManager notificationmanager = (NotificationManager) getActivity()
					.getSystemService(getActivity().NOTIFICATION_SERVICE);
			notificationmanager.notify(0, builder.build());

		}

		private void createCallXLSX() {

			String name, number, type, duration, date, dateTime;
			long id;
			int counter = 1;
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Call Log Data");

			Map<String, Object[]> data = new TreeMap<String, Object[]>();

			data.put("" + counter, new Object[] { "ID", "NAME", "NUMBER",
					"TYPE", "DURATION", "DATE", "DATE_TIME" });

			for (callCursor.moveToFirst(); !callCursor.isAfterLast(); callCursor
					.moveToNext()) {

				id = callCursor
						.getLong(callCursor.getColumnIndex(DBAdapter.ID));
				name = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.NAME));
				number = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				type = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_TYPE));
				duration = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_DURATION));
				date = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_DATE));

				dateTime = callCursor.getString(callCursor
						.getColumnIndex(DBAdapter.CALL_DATE_TIME));

				data.put("" + (counter++), new Object[] { "" + id, name,
						number, type, duration, date, dateTime });

			}

			Set<String> keyset = data.keySet();
			int rownum = 0;
			for (String key : keyset) {
				Row row = sheet.createRow(rownum++);
				Object[] objArr = data.get(key);
				int cellnum = 0;
				for (Object obj : objArr) {
					Cell cell = row.createCell(cellnum++);
					if (obj instanceof String) {
						cell.setCellValue((String) obj);
					} else if (obj instanceof Integer) {
						cell.setCellValue((Integer) obj);
					}
				}
			}
			try {
				File myDirectory = new File("/sdcard/Log_Book/call");
				myDirectory.mkdirs();

				File outputFile = new File(myDirectory, "Call_History.xlsx");
				FileOutputStream fos = new FileOutputStream(outputFile);

				workbook.write(fos);
				fos.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class CreateSMSXLSX extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "Creating Backup",
					"Please Wait....", true);
		}

		@Override
		protected Void doInBackground(Void... arguments) {

			createSMSXLSX();
			return null;
		}

		public void createSMSNotification() {
			String strtitle = getString(R.string.notificationtitle);

			String strtext = getString(R.string.notificationtext);

			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_GET_CONTENT);
			File file = new File("/sdcard/Log_Book/sms");
			intent.setDataAndType(Uri.fromFile(file), "application/file");

			intent.putExtra("title", strtitle);
			intent.putExtra("text", strtext);

			PendingIntent pIntent = PendingIntent.getActivity(getActivity()
					.getApplicationContext(), 0, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					getActivity().getApplicationContext())
					.setSmallIcon(R.drawable.calllog_small_ic)
					.setTicker(getString(R.string.notificationticker))
					.setContentTitle(getString(R.string.notificationtitle))
					.setContentText(getString(R.string.notificationtext))
					.addAction(R.drawable.excel_bck_ic, "Tap to view", pIntent)
					.setContentIntent(pIntent).setAutoCancel(true);

			NotificationManager notificationmanager = (NotificationManager) getActivity()
					.getSystemService(getActivity().NOTIFICATION_SERVICE);
			notificationmanager.notify(0, builder.build());

		}

		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();
			Toast.makeText(getActivity(), "created", Toast.LENGTH_SHORT).show();
			createSMSNotification();
		}

		private void createSMSXLSX() {

			long id;
			int counter = 1;
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("SMS Log Data");

			Map<String, Object[]> data = new TreeMap<String, Object[]>();

			data.put("" + counter, new Object[] { "ID", "NAME", "NUMBER",
					"BODY", "DATE", "TYPE" });

			for (smsCursor.moveToFirst(); !smsCursor.isAfterLast(); smsCursor
					.moveToNext()) {

				id = smsCursor.getLong(smsCursor.getColumnIndex(DBAdapter.ID));
				String name = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.NAME));
				String number = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));
				String body = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_BODY));
				String type = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_TYPE));
				String date = smsCursor.getString(smsCursor
						.getColumnIndex(DBAdapter.SMS_DATE));

				data.put("" + (counter++), new Object[] { "" + id, name, number,
						body, date, type });

			}

			Set<String> keyset = data.keySet();
			int rownum = 0;
			for (String key : keyset) {
				Row row = sheet.createRow(rownum++);
				Object[] objArr = data.get(key);
				int cellnum = 0;
				for (Object obj : objArr) {
					Cell cell = row.createCell(cellnum++);
					if (obj instanceof String) {
						cell.setCellValue((String) obj);
					} else if (obj instanceof Integer) {
						cell.setCellValue((Integer) obj);
					}
				}
			}
			try {
				File myDirectory = new File("/sdcard/Log_Book/sms");
				myDirectory.mkdirs();

				File outputFile = new File(myDirectory, "SMS_History.xlsx");
				FileOutputStream fos = new FileOutputStream(outputFile);

				workbook.write(fos);
				fos.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
