package com.cabez.logbook.main;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.calllogadapter.CallArrayAdapter;
import com.cabez.calllogadapter.ContactCall;
import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class FragmentAllCalls extends Fragment {

	ImageView ivIcon;
	TextView tvItemName;

	private DBAdapter dbAdapter;
	ListView listview;
	CallArrayAdapter callArrayAdapter;
	ArrayList<ContactCall> callArray = new ArrayList<ContactCall>();

	Cursor cursor;

	public FragmentAllCalls() {}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_one, container,
				false);

		dbCreate();
		
		cursor = dbAdapter.getAllCallLog();
		ContactCall.setCursor(cursor);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

			String number = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_NUM));
			String name = cursor.getString(cursor
					.getColumnIndex(DBAdapter.NAME));
			String callDayTime = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_DATE));
			String duration = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_DURATION));
			String type = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_TYPE));

			callArray.add(new ContactCall(name, number, "" + callDayTime,
					duration, type));
		}

		callArrayAdapter = new CallArrayAdapter(getActivity(),
				R.layout.call_log_row, callArray);

		listview = (ListView) view.findViewById(R.id.lv);
		listview.setItemsCanFocus(false);
		listview.setAdapter(callArrayAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					final int position, long id) {

				Toast.makeText(getActivity(), "List Item Clicked:" + position,
						Toast.LENGTH_LONG).show();

				cursor.moveToPosition(position);
				String nmbr = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:" + nmbr));
				startActivity(callIntent);
				
			}
		});

		return view;
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(getActivity());
		dbAdapter.open();
	}

}
