package com.cabez.logbook.main;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteMisuseException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract.PhoneLookup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;

public class FragmentReload extends Fragment {

	Cursor cursor;
	DBAdapter db;
	ImageView emoji;
	TextView done;
	ProgressDialog pd;
	Cursor callCursor, smsCursor;
	Button yes, no;
	int counter = 0;

	public FragmentReload() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_ten, container,
				false);
		emoji = (ImageView) view.findViewById(R.id.emoji);
		done = (TextView) view.findViewById(R.id.done);
		emoji.setVisibility(View.GONE);
		done.setVisibility(View.GONE);
		if (counter == 0) {
			final Dialog d = new Dialog(getActivity());
			d.setTitle("Reload");
			d.requestWindowFeature(Window.FEATURE_NO_TITLE);
			d.getWindow().setBackgroundDrawable(
					new ColorDrawable(Color.TRANSPARENT));
			d.setContentView(R.layout.dialog_reload);
			d.setCancelable(false);
			yes = (Button) d.findViewById(R.id.yesReload);
			no = (Button) d.findViewById(R.id.noReload);
			yes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					new AsyncLogDbCreator().execute();
					d.dismiss();
				}
			});
			no.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					d.dismiss();
					emoji.setVisibility(View.VISIBLE);
					done.setVisibility(View.VISIBLE);
					emoji.setBackgroundResource(R.drawable.ic_sad);
					done.setText("Canceled");
				}
			});
			counter++;
			d.show();
		}
		return view;
	}

	class AsyncLogDbCreator extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(getActivity(), "Categorizing Your System",
					"Processing...Please Wait", true);

		}

		@Override
		protected Void doInBackground(Void... arguments) {

			db = new DBAdapter(getActivity());
			db.open();
			getLogs();

			copyCallLog();
			copySMSlog();

			return null;
		}

		private void getLogs() {

			callCursor = getActivity().getContentResolver().query(
					CallLog.Calls.CONTENT_URI,
					new String[] { CallLog.Calls.NUMBER,
							CallLog.Calls.CACHED_NAME, CallLog.Calls.TYPE,
							CallLog.Calls.DATE, CallLog.Calls.DURATION }, null,
					null, CallLog.Calls.DATE + " DESC");

			final Uri uri = Uri.parse("content://sms");
			smsCursor = getActivity().getContentResolver().query(uri, null,
					null, null, null);
		}

		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();
			emoji.setVisibility(View.VISIBLE);
			done.setVisibility(View.VISIBLE);
			emoji.setBackgroundResource(R.drawable.ic_happy);
			done.setText("Done");
			Toast.makeText(getActivity(), "System is Ready To Use",
					Toast.LENGTH_SHORT).show();
		}

		private void copyCallLog() {

			int i = 0;
			for (callCursor.moveToFirst(); !callCursor.isAfterLast(); callCursor
					.moveToNext(), i++) {

				String number = callCursor.getString(callCursor
						.getColumnIndex(CallLog.Calls.NUMBER));

				String name = callCursor.getString(callCursor
						.getColumnIndex(CallLog.Calls.CACHED_NAME));

				/**/
				if (!number.contains("+")) {
					Uri uri = Uri.withAppendedPath(
							PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
					Cursor contactCursor = getActivity().getContentResolver()
							.query(uri,
									new String[] { PhoneLookup.DISPLAY_NAME },
									number, null, null);
					if (contactCursor.getCount() > 0) {
						contactCursor.moveToFirst();
						name = contactCursor.getString(contactCursor
								.getColumnIndex(PhoneLookup.DISPLAY_NAME));
					} else {
						name = callCursor.getString(callCursor
								.getColumnIndex(CallLog.Calls.NUMBER));
					}

				} else if (number.contains("+")) {
					String tempNumber = number.substring(3);
					Uri uri = Uri.withAppendedPath(
							PhoneLookup.CONTENT_FILTER_URI,
							Uri.encode(tempNumber));
					Cursor contactCursor = getActivity().getContentResolver()
							.query(uri,
									new String[] { PhoneLookup.DISPLAY_NAME },
									tempNumber, null, null);
					if (contactCursor.getCount() > 0) {
						contactCursor.moveToFirst();
						name = contactCursor.getString(contactCursor
								.getColumnIndex(PhoneLookup.DISPLAY_NAME));
					} else {
						name = callCursor.getString(callCursor
								.getColumnIndex(CallLog.Calls.NUMBER));

					}
				}
				/**/
				String callType = callCursor.getString(callCursor
						.getColumnIndex(CallLog.Calls.TYPE));

				String callDate = callCursor.getString(callCursor
						.getColumnIndex(android.provider.CallLog.Calls.DATE));

				SimpleDateFormat formatter = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm");
				String dateString = formatter.format(new Date(Long
						.parseLong(callDate)));

				long durationMillis = callCursor.getLong(callCursor
						.getColumnIndex(CallLog.Calls.DURATION));
				String duration = "" + durationMillis;

				String type = null;
				int callTypeCode = Integer.parseInt(callType);

				switch (callTypeCode) {
				case CallLog.Calls.OUTGOING_TYPE:
					type = "OUTGOING";
					break;

				case CallLog.Calls.INCOMING_TYPE:
					type = "INCOMING";
					break;

				case CallLog.Calls.MISSED_TYPE:
					type = "MISSED";
					break;
				}
				try {
					db.createCalldb(i, number, name, dateString, type,
							duration, callDate);
				} catch (SQLiteAbortException e) {

					continue;
				} catch (SQLiteDoneException e) {
					continue;
				} catch (SQLiteMisuseException e) {

					continue;
				} catch (SQLiteConstraintException e) {

					continue;
				} catch (SQLiteException error) {

					continue;
				}
			}
		}

		private void copySMSlog() {

			if (smsCursor.moveToFirst()) {
				for (int i = 0; i < smsCursor.getCount(); i++) {
					String body = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("body")).toString();
					String number = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("address"))
							.toString();
					String name = "";
					if (!number.contains("+")) {
						Uri uri = Uri.withAppendedPath(
								PhoneLookup.CONTENT_FILTER_URI,
								Uri.encode(number));
						Cursor contactCursor = getActivity()
								.getContentResolver()
								.query(uri,
										new String[] { PhoneLookup.DISPLAY_NAME },
										number, null, null);
						if (contactCursor.getCount() > 0) {
							contactCursor.moveToFirst();
							name = contactCursor.getString(contactCursor
									.getColumnIndex(PhoneLookup.DISPLAY_NAME));
						} else {
							name = number;
						}

					} else if (number.contains("+")) {
						String tempNumber = number.substring(3);
						Uri uri = Uri.withAppendedPath(
								PhoneLookup.CONTENT_FILTER_URI,
								Uri.encode(tempNumber));
						Cursor contactCursor = getActivity()
								.getContentResolver()
								.query(uri,
										new String[] { PhoneLookup.DISPLAY_NAME },
										tempNumber, null, null);
						if (contactCursor.getCount() > 0) {
							contactCursor.moveToFirst();
							name = contactCursor.getString(contactCursor
									.getColumnIndex(PhoneLookup.DISPLAY_NAME));
						} else {
							name = number;
						}
					}

					String date = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("date")).toString();

					String type = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("type")).toString();
					String typeOfSMS = null;
					switch (Integer.parseInt(type)) {
					case 1:
						typeOfSMS = "INBOX";
						break;

					case 2:
						typeOfSMS = "SENT";
						break;

					case 3:
						typeOfSMS = "DRAFT";
						break;
					}
					try {
						db.createSMSdb(number, name, body, typeOfSMS, date);
					} catch (SQLiteAbortException e) {

						continue;
					} catch (SQLiteDoneException e) {

						continue;
					} catch (SQLiteMisuseException e) {

						continue;
					} catch (SQLiteConstraintException e) {

						continue;
					} catch (SQLiteException error) {

						continue;
					}
					smsCursor.moveToNext();
				}

			}
			smsCursor.close();
		}
	}

}
