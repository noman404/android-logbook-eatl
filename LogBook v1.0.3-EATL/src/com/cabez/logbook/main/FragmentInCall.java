package com.cabez.logbook.main;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.cabez.loglistadapter.inOutMissedCallLog.CallInOutMissArrayAdapter;
import com.cabez.loglistadapter.inOutMissedCallLog.ContactCallInOutMissed;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class FragmentInCall extends Fragment {

	ImageView ivIcon;
	TextView tvItemName;

	ListView listview;
	CallInOutMissArrayAdapter inCallArrayAdapter;
	ArrayList<ContactCallInOutMissed> inCallArray = new ArrayList<ContactCallInOutMissed>();
	private DBAdapter dbAdapter;
	Cursor cursor;

	public FragmentInCall() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_three, container,
				false);
		
		dbCreate();

		
		cursor = dbAdapter.getInCallLog();
		ContactCallInOutMissed.setCursor(cursor);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

			String number = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_NUM));
			String name = cursor.getString(cursor
					.getColumnIndex(DBAdapter.NAME));
			String callDayTime = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_DATE));
			String duration = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_DURATION));
			
			inCallArray.add(new ContactCallInOutMissed(name, number, "" + callDayTime, duration));
		}

		inCallArrayAdapter = new CallInOutMissArrayAdapter(getActivity(),
				R.layout.list_item, inCallArray);

		listview = (ListView) view.findViewById(R.id.lv3);
		listview.setItemsCanFocus(false);
		listview.setAdapter(inCallArrayAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					final int position, long id) {

				Toast.makeText(getActivity(), "List Item Clicked:" + position,
						Toast.LENGTH_LONG).show();

				cursor.moveToPosition(position);
				String num = cursor.getString(cursor.getColumnIndex(DBAdapter.CALL_NUM));
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:"+num));
				startActivity(callIntent);
			}
		});

		return view;
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(getActivity());
		dbAdapter.open();
	}


}
