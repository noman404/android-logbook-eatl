package com.cabez.logbook.main;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.cabez.loglistadapter.inOutMissedCallLog.CallInOutMissArrayAdapter;
import com.cabez.loglistadapter.inOutMissedCallLog.ContactCallInOutMissed;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class FragmentMissedCall extends Fragment {

	ListView listview;
	CallInOutMissArrayAdapter missedCallArrayAdapter;
	ArrayList<ContactCallInOutMissed> inCallArray = new ArrayList<ContactCallInOutMissed>();
	private DBAdapter dbAdapter;
	Cursor cursor;

	public FragmentMissedCall() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_five, container,
				false);

		dbCreate();

		cursor = dbAdapter.getMissedCallLog();
		ContactCallInOutMissed.setCursor(cursor);

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

			String number = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_NUM));
			String name = cursor.getString(cursor
					.getColumnIndex(DBAdapter.NAME));
			String callDayTime = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_DATE));
			String type = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_TYPE));

			String duration = cursor.getString(cursor
					.getColumnIndex(DBAdapter.CALL_DURATION));

			inCallArray.add(new ContactCallInOutMissed(name, number, ""
					+ callDayTime, duration));
		}

		missedCallArrayAdapter = new CallInOutMissArrayAdapter(getActivity(),
				R.layout.list_item, inCallArray);

		listview = (ListView) view.findViewById(R.id.lv5);
		listview.setItemsCanFocus(false);
		listview.setAdapter(missedCallArrayAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					final int position, long id) {

				Toast.makeText(getActivity(), "List Item Clicked:" + position,
						Toast.LENGTH_LONG).show();

				cursor.moveToPosition(position);
				String num = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:" + num));
				startActivity(callIntent);
			}
		});

		return view;
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(getActivity());
		dbAdapter.open();
	}

}
