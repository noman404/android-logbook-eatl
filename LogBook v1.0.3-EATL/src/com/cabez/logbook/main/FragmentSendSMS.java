package com.cabez.logbook.main;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.cabez.inappmessenger.SendMessage;
import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;
import com.cabez.smslogadapter.ContactSMS;
import com.cabez.smslogadapter.SMSArrayAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class FragmentSendSMS extends Fragment {

	ListView listview;
	SMSArrayAdapter smsArrayAdapter;
	ArrayList<ContactSMS> smsArray = new ArrayList<ContactSMS>();
	private DBAdapter dbAdapter;
	Cursor cursor;

	public FragmentSendSMS() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_layout_seven, container,
				false);

		dbCreate();
		

		cursor = dbAdapter.getSendSMSLog();
		ContactSMS.setCursor(cursor);

		if (cursor.moveToFirst()) {
			for (int i = 0; i < cursor.getCount(); i++) {
				String number = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));
				String body = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_BODY));
				String type = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_TYPE));
				String name = cursor.getString(cursor
						.getColumnIndex(DBAdapter.NAME));
				String date = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_DATE));
				SimpleDateFormat formatter = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm");
				String dateString = formatter.format(new Date(Long
						.parseLong(date)));

				smsArray.add(new ContactSMS(name, number, body, type, dateString));
				cursor.moveToNext();
			}
		}

		smsArrayAdapter = new SMSArrayAdapter(getActivity(),
				R.layout.list_item, smsArray);

		listview = (ListView) view.findViewById(R.id.lv7);
		listview.setItemsCanFocus(false);
		listview.setAdapter(smsArrayAdapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					final int position, long id) {

				cursor.moveToPosition(position);
				String num = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));

				Intent intent = new Intent(getActivity(), SendMessage.class);
				intent.putExtra("NUMBER", num);
				startActivity(intent);
				Toast.makeText(getActivity(), "List Item Clicked:" + position,
						Toast.LENGTH_LONG).show();

			}
		});

		return view;
	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(getActivity());
		dbAdapter.open();
	}

}
