package com.cabez.logbook.main;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteMisuseException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;

public class MainActivity extends Activity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	CustomDrawerAdapter adapter;

	public DBAdapter db;
	List<DrawerItem> dataList;

	ProgressDialog pd;
	Cursor callCursor, smsCursor;

	Dialog dialog;
	boolean isDialog = true;

	public static final String MyPREFERENCES = "logBookPref";
	SharedPreferences sharedpreferences;
	SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		overridePendingTransition(R.anim.up, R.anim.down);

		sharedpreferences = getSharedPreferences(MyPREFERENCES,
				MainActivity.MODE_PRIVATE);
		editor = sharedpreferences.edit();

		dataList = new ArrayList<DrawerItem>();
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		// drwr_List
		dataList.add(new DrawerItem("Call Log", R.drawable._phone)); // 0
		dataList.add(new DrawerItem("SMS log", R.drawable._sms)); // 1
		dataList.add(new DrawerItem("Incoming Calls", R.drawable._phone)); // 2
		dataList.add(new DrawerItem("Outgoing Calls", R.drawable._phone)); // 3
		dataList.add(new DrawerItem("Missed Calls", R.drawable._phone)); // 4
		dataList.add(new DrawerItem("Send SMS", R.drawable._sms)); // 5
		dataList.add(new DrawerItem("Inboxed SMS", R.drawable._sms)); // 6
		dataList.add(new DrawerItem("Draft SMS", R.drawable._sms)); // 7
		dataList.add(new DrawerItem("Import & Export", R.drawable._stack)); // 8
		dataList.add(new DrawerItem("Advanced Search", R.drawable._search));// 9
		dataList.add(new DrawerItem("Reload Logs", R.drawable._reoad));// 10

		adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
				dataList);

		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			SelectItem(0);
		}

		new AsyncLogDbCreator().execute();

	}

	public void SelectItem(int possition) {

		Fragment fragment = null;
		Bundle args = new Bundle();

		switch (possition) {
		case 0:
			fragment = new FragmentAllCalls();

			break;
		case 1:
			fragment = new FragmentAllSMS();

			break;
		case 2:
			fragment = new FragmentInCall();

			break;
		case 3:
			fragment = new FragmentOutCall();

			break;
		case 4:
			fragment = new FragmentMissedCall();

			break;
		case 5:
			fragment = new FragmentSendSMS();

			break;
		case 6:
			fragment = new FragmentInboxedSMS();

			break;
		case 7:
			fragment = new FragmentDraftSMS();

			break;
		case 8:
			fragment = new FragmentExportImport();

			break;
		case 9:
			fragment = new FragmentSearch();

			break;
		case 10:
			fragment = new FragmentReload();

			break;
		default:
			fragment = new FragmentAllCalls();

			break;
		}

		fragment.setArguments(args);
		FragmentManager frgManager = getFragmentManager();
		frgManager.beginTransaction().replace(R.id.content_frame, fragment)
				.commit();

		mDrawerList.setItemChecked(possition, true);
		setTitle(dataList.get(possition).getItemName());
		mDrawerLayout.closeDrawer(mDrawerList);

	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return false;
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			SelectItem(position);

		}
	}

	class AsyncLogDbCreator extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			pd = ProgressDialog.show(MainActivity.this,
					"Categorizing Your System", "Processing...Please Wait",
					true);
		}

		@Override
		protected Void doInBackground(Void... arguments) {

			db = new DBAdapter(MainActivity.this);
			db.open();

			if (db.isCalldbEmpty() || db.isSMSdbEmpty()) {
				getLogs();
				copyCallLog();
				copySMSlog();
			}

			return null;
		}

		private void getLogs() {

			callCursor = getContentResolver().query(
					CallLog.Calls.CONTENT_URI,
					new String[] { CallLog.Calls.NUMBER,
							CallLog.Calls.CACHED_NAME, CallLog.Calls.TYPE,
							CallLog.Calls.DATE, CallLog.Calls.DURATION }, null,
					null, CallLog.Calls.DATE + " DESC");

			final Uri uri = Uri.parse("content://sms");
			smsCursor = getContentResolver().query(uri, null, null, null, null);
		}

		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Void result) {
			pd.dismiss();
			Toast.makeText(MainActivity.this, "System is Ready To use",
					Toast.LENGTH_SHORT).show();

			isDialog = sharedpreferences.getBoolean("dialog", true);

			if (isDialog) {
				dialog = new Dialog(MainActivity.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				getWindow().setLayout(LayoutParams.FILL_PARENT,
						LayoutParams.FILL_PARENT);
				dialog.setContentView(R.layout.first_dialog);
				dialog.setCancelable(false);
				Button gotit = (Button) dialog.findViewById(R.id.gotit);
				gotit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						editor = sharedpreferences.edit();
						editor.clear();
						isDialog = false;
						editor.putBoolean("dialog", isDialog);
						editor.commit();

						dialog.dismiss();
					}
				});

				dialog.show();
			}

		}

		private void copyCallLog() {

			int i = 0;
			for (callCursor.moveToFirst(); !callCursor.isAfterLast(); callCursor
					.moveToNext(), i++) {

				String number = callCursor.getString(callCursor
						.getColumnIndex(CallLog.Calls.NUMBER));

				String name = callCursor.getString(callCursor
						.getColumnIndex(CallLog.Calls.CACHED_NAME));
				/* get name from contacts by number */
				if (!number.contains("+")) {
					Uri uri = Uri.withAppendedPath(
							PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
					Cursor contactCursor = getContentResolver().query(uri,
							new String[] { PhoneLookup.DISPLAY_NAME }, number,
							null, null);
					if (contactCursor.getCount() > 0) {
						contactCursor.moveToFirst();
						name = contactCursor.getString(contactCursor
								.getColumnIndex(PhoneLookup.DISPLAY_NAME));
					} else {
						name = callCursor.getString(callCursor
								.getColumnIndex(CallLog.Calls.NUMBER));
					}

				} else if (number.contains("+")) {
					String tempNumber = number.substring(3);
					Uri uri = Uri.withAppendedPath(
							PhoneLookup.CONTENT_FILTER_URI,
							Uri.encode(tempNumber));
					Cursor contactCursor = getContentResolver().query(uri,
							new String[] { PhoneLookup.DISPLAY_NAME },
							tempNumber, null, null);
					if (contactCursor.getCount() > 0) {
						contactCursor.moveToFirst();
						name = contactCursor.getString(contactCursor
								.getColumnIndex(PhoneLookup.DISPLAY_NAME));
					} else {
						name = callCursor.getString(callCursor
								.getColumnIndex(CallLog.Calls.NUMBER));

					}
				}
				/*****/
				String callType = callCursor.getString(callCursor
						.getColumnIndex(CallLog.Calls.TYPE));

				String callDate = callCursor.getString(callCursor
						.getColumnIndex(android.provider.CallLog.Calls.DATE));

				SimpleDateFormat formatter = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm");
				String dateString = formatter.format(new Date(Long
						.parseLong(callDate)));

				long durationMillis = callCursor.getLong(callCursor
						.getColumnIndex(CallLog.Calls.DURATION));
				String duration = "" + durationMillis;

				String type = null;
				int callTypeCode = Integer.parseInt(callType);

				switch (callTypeCode) {
				case CallLog.Calls.OUTGOING_TYPE:
					type = "OUTGOING";
					break;

				case CallLog.Calls.INCOMING_TYPE:
					type = "INCOMING";
					break;

				case CallLog.Calls.MISSED_TYPE:
					type = "MISSED";
					break;
				}
				try {
					db.createCalldb(i, number, name, dateString, type,
							duration, callDate);
				} catch (SQLiteAbortException e) {

					continue;
				} catch (SQLiteDoneException e) {

					continue;
				} catch (SQLiteMisuseException e) {

					continue;
				} catch (SQLiteConstraintException e) {

					continue;
				} catch (SQLiteException error) {

					continue;
				} catch (Exception e) {

					continue;
				}
			}
		}

		private void copySMSlog() {

			if (smsCursor.moveToFirst()) {
				for (int i = 0; i < smsCursor.getCount(); i++) {
					String body = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("body")).toString();
					String number = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("address"))
							.toString();

					String name = "";
					if (!number.contains("+")) {
						Uri uri = Uri.withAppendedPath(
								PhoneLookup.CONTENT_FILTER_URI,
								Uri.encode(number));
						Cursor contactCursor = getContentResolver().query(uri,
								new String[] { PhoneLookup.DISPLAY_NAME },
								number, null, null);
						if (contactCursor.getCount() > 0) {
							contactCursor.moveToFirst();
							name = contactCursor.getString(contactCursor
									.getColumnIndex(PhoneLookup.DISPLAY_NAME));
						} else {
							name = number;
						}

					} else if (number.contains("+")) {
						String tempNumber = number.substring(3);
						Uri uri = Uri.withAppendedPath(
								PhoneLookup.CONTENT_FILTER_URI,
								Uri.encode(tempNumber));
						Cursor contactCursor = getContentResolver().query(uri,
								new String[] { PhoneLookup.DISPLAY_NAME },
								tempNumber, null, null);
						if (contactCursor.getCount() > 0) {
							contactCursor.moveToFirst();
							name = contactCursor.getString(contactCursor
									.getColumnIndex(PhoneLookup.DISPLAY_NAME));
						} else {
							name = number;
						}
					}

					String date = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("date")).toString();

					String type = smsCursor.getString(
							smsCursor.getColumnIndexOrThrow("type")).toString();
					String typeOfSMS = null;
					switch (Integer.parseInt(type)) {
					case 1:
						typeOfSMS = "INBOX";
						break;

					case 2:
						typeOfSMS = "SENT";
						break;

					case 3:
						typeOfSMS = "DRAFT";
						break;
					}
					try {
						db.createSMSdb(number, name, body, typeOfSMS, date);
					} catch (SQLiteAbortException e) {

						continue;
					} catch (SQLiteDoneException e) {

						continue;
					} catch (SQLiteMisuseException e) {

						continue;
					} catch (SQLiteConstraintException e) {

						continue;
					} catch (SQLiteException error) {

						continue;
					} catch (Exception e) {

						continue;
					}
					smsCursor.moveToNext();
				}
			}
			smsCursor.close();
		}
	}

	@Override
	protected void onPause() {
		if (pd.isShowing() || pd != null) {
			pd.dismiss();
		}
		super.onPause();
	}

}
