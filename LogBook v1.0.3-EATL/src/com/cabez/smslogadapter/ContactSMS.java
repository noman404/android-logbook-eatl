package com.cabez.smslogadapter;

import android.content.ContentResolver;
import android.database.Cursor;

public class ContactSMS {
	private String name;
	private String number;
	private String date;
	private String type;
	private String savedName;

	private static ContentResolver cr;

	private static Cursor cursor;

	public ContactSMS(String savedName,String name, String number, String type, String date) {
		super();
		this.name = name;
		this.number = number;
		this.date = date;
		this.type = type;
		this.savedName = savedName;
	}

	public static ContentResolver getCr() {
		return cr;
	}

	public static void setCr(ContentResolver cr) {
		ContactSMS.cr = cr;
	}

	public static Cursor getCursor() {
		return cursor;
	}

	public static void setCursor(Cursor cursor) {
		ContactSMS.cursor = cursor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String age) {
		this.number = age;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSavedName() {
		return savedName;
	}

	public void setSavedName(String savedName) {
		this.savedName = savedName;
	}
	
	
}
