package com.cabez.smslogadapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.inappmessenger.SendMessage;
import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;

public class SMSArrayAdapter extends ArrayAdapter<ContactSMS> {

	Context context;
	int layoutResourceId;
	public DBAdapter dbAdapter;
	ArrayList<ContactSMS> sms = new ArrayList<ContactSMS>();
	Cursor cursor;

	public SMSArrayAdapter(Context context, int layoutResourceId,
			ArrayList<ContactSMS> sms) {
		super(context, layoutResourceId, sms);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.sms = sms;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		ViewWrapper viewWrapper = null;

		if (item == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			item = inflater.inflate(layoutResourceId, parent, false);
			viewWrapper = new ViewWrapper();
			viewWrapper.body = (TextView) item.findViewById(R.id.textName);
			viewWrapper.num = (TextView) item.findViewById(R.id.textAge);
			viewWrapper.type = (TextView) item.findViewById(R.id.textAddr);
			viewWrapper.edit = (Button) item.findViewById(R.id.btnEdit);
			viewWrapper.delete = (Button) item.findViewById(R.id.btnDelete);
			viewWrapper.date = (TextView) item.findViewById(R.id.textDur);
			viewWrapper.phn = (Button) item.findViewById(R.id.btnPhn);
			viewWrapper.sms = (Button) item.findViewById(R.id.btnMsg);
			item.setTag(viewWrapper);
		} else {
			viewWrapper = (ViewWrapper) item.getTag();
		}

		ContactSMS cSMS = sms.get(position);
		viewWrapper.body.setText(cSMS.getSavedName());
		viewWrapper.num.setText(cSMS.getName());
		viewWrapper.date.setText(cSMS.getDate());
		viewWrapper.type.setText(cSMS.getType());
		cursor = ContactSMS.getCursor();
		dbCreate();
		final int pos = position;

		viewWrapper.edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				cursor.moveToPosition(pos);
				String number = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));

				Dialog d = new Dialog(getContext());
				d.setTitle(number + " History");
				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));

				d.setContentView(R.layout.detail_dialog);
				TextView t = (TextView) d.findViewById(R.id.detail);

				t.append("Total Inbox: " + dbAdapter.getTotalInbox(number)
						+ "\n");
				t.append("Total Send: " + dbAdapter.getTotalOutbox(number));

				d.show();
			}
		});
		viewWrapper.delete.setVisibility(View.GONE);
		viewWrapper.phn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String number = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));

				try {
					String uri = "tel:" + number;
					Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(uri));

					context.startActivity(dialIntent);

				} catch (Exception e) {
					Toast.makeText(context.getApplicationContext(),
							"Your call has failed...", Toast.LENGTH_LONG)
							.show();

				}
			}
		});

		viewWrapper.sms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cursor.moveToPosition(pos);
				String num = cursor.getString(cursor
						.getColumnIndex(DBAdapter.SMS_NUMBER));

				Intent intent = new Intent(context, SendMessage.class);
				intent.putExtra("NUMBER", num);
				context.startActivity(intent);
			}
		});

		return item;

	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(context);
		dbAdapter.open();
	}

	static class ViewWrapper {
		TextView body;
		TextView num;
		TextView type;
		TextView date;
		Button phn;
		Button sms;
		Button edit;
		Button delete;
	}
}
