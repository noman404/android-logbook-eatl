package com.cabez.calllogadapter;


import android.content.ContentResolver;
import android.database.Cursor;

public class ContactCall {
	private String name;
	private String number;
	private String date;
	private String dur;
	private String type;

	private static ContentResolver cr;
	
	private static Cursor cursor;

	public ContactCall(String name, String number, String date, String dur, String type) {
		super();
		this.name = name;
		this.number = number;
		this.date = date;
		this.dur = dur;
		this.type = type;
	}
	public static ContentResolver getCr() {
		return cr;
	}
	public static void setCr(ContentResolver cr) {
		ContactCall.cr = cr;
	}

	public static Cursor getCursor() {
		return cursor;
	}

	public static void setCursor(Cursor cursor) {
		ContactCall.cursor = cursor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String age) {
		this.number = age;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	public String getDur() {
		return dur;
	}
	public void setDur(String dur) {
		this.dur = dur;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}


}