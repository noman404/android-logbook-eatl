package com.cabez.calllogadapter;

import java.text.ParseException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cabez.callsbarchart.BarGraphCreator;
import com.cabez.inappmessenger.SendMessage;
import com.cabez.logbook.R;
import com.cabez.logbook.db.DBAdapter;

public class CallArrayAdapter extends ArrayAdapter<ContactCall> {

	Context context;
	int layoutResourceId;
	ArrayList<ContactCall> calls = new ArrayList<ContactCall>();
	Cursor crsr, cursor;
	private DBAdapter dbAdapter;

	public CallArrayAdapter(Context context, int layoutResourceId,
			ArrayList<ContactCall> calls) {
		super(context, layoutResourceId, calls);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.calls = calls;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		ViewWrapper viewWrapper = null;

		if (item == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			item = inflater.inflate(layoutResourceId, parent, false);
			viewWrapper = new ViewWrapper();
			viewWrapper.name = (TextView) item.findViewById(R.id.textNameCall1);
			viewWrapper.num = (TextView) item.findViewById(R.id.textAgeCall1);
			viewWrapper.date = (TextView) item.findViewById(R.id.textAddrCall1);
			viewWrapper.detail = (Button) item.findViewById(R.id.btnEditCall1);
			viewWrapper.graph = (Button) item.findViewById(R.id.btnDeleteCall1);
			viewWrapper.phn = (Button) item.findViewById(R.id.btnPhnCall1);
			viewWrapper.sms = (Button) item.findViewById(R.id.btnMsgCall1);
			viewWrapper.dur = (TextView) item.findViewById(R.id.typeCall1);
			viewWrapper.type = (TextView) item
					.findViewById(R.id.timeDurationCall1);

			item.setTag(viewWrapper);
		} else {
			viewWrapper = (ViewWrapper) item.getTag();
		}

		ContactCall callLog = calls.get(position);
		viewWrapper.name.setText(callLog.getName());
		viewWrapper.num.setText(callLog.getNumber());
		viewWrapper.date.setText(callLog.getDate());
		viewWrapper.dur.setText(callLog.getDur() + " Sec");
		viewWrapper.type.setText(callLog.getType());

		cursor = ContactCall.getCursor();
		final int pos = position;
		dbCreate();

		viewWrapper.detail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				cursor.moveToPosition(pos);
				String nmbr = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				Dialog d = new Dialog(getContext());
				d.setTitle(nmbr + "'s History");
				d.requestWindowFeature(Window.FEATURE_NO_TITLE);
				d.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				d.setContentView(R.layout.detail_dialog);
				TextView text = (TextView) d.findViewById(R.id.detail);

				Cursor cr = dbAdapter.getCallLogByNumber(nmbr);
				for (cr.moveToFirst(); !cr.isAfterLast(); cr.moveToNext()) {
					text.append(cr.getString(cr
							.getColumnIndex(DBAdapter.CALL_DATE)) + "\n");
				}
				text.append("Total Calls: " + cr.getCount() + " Times");

				d.show();
			}
		});

		viewWrapper.graph.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String nmbr = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));
				BarGraphCreator barChart = new BarGraphCreator();
				Intent barIntent;
				try {
					barIntent = barChart.getIntent(context, nmbr);
					context.startActivity(barIntent);

				} catch (ParseException e) {
					Toast.makeText(context, "ERROR in generating Graph",
							Toast.LENGTH_SHORT).show();
				}
				Toast.makeText(context, "Graph for: " + nmbr, Toast.LENGTH_LONG)
						.show();

			}
		});

		viewWrapper.phn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String number = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));

				try {
					String uri = "tel:" + number;
					Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(uri));

					context.startActivity(dialIntent);

				} catch (Exception e) {

					Toast.makeText(context.getApplicationContext(),
							"Your call has failed...", Toast.LENGTH_LONG)
							.show();

				}

			}
		});

		viewWrapper.sms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cursor.moveToPosition(pos);
				String nmbr = cursor.getString(cursor
						.getColumnIndex(DBAdapter.CALL_NUM));

				Intent intent = new Intent(context.getApplicationContext(),
						SendMessage.class);
				intent.putExtra("NUMBER", nmbr);
				context.startActivity(intent);

			}
		});

		return item;

	}

	private void dbCreate() {

		dbAdapter = new DBAdapter(context);
		dbAdapter.open();
	}

	static class ViewWrapper {
		TextView name;
		TextView num;
		TextView date;
		TextView dur;
		TextView type;
		Button phn;
		Button graph;
		Button sms;
		Button detail;

	}

}
